#!/bin/bash

dossier=$(readlink -f $1)
echo $dossier > $dossier/chemin.txt
archive=/tmp/backup/$(date +%Y_%m_%d_%H%M)_$(basename $1).tar.gz

tar -cvzf $archive $1

