#!/bin/bash

if [ "$(id -u)" != "0" ]; then
	echo "[/!\] vous devez etre super-utilisateur[/!\]"
	exit 1 
else
	echo "[...] update database  [...]"
	apt-get -qq update
	echo "[...] upgrade system   [...]"
	apt-get -qq upgrade
fi
