#!/bin/bash
	echo "[...] Checking Internet connection[...]"
wget -q --tries=10 --timeout=20 --spider http://google.com
if [[ $? -eq 0 ]]; then
	echo "[...] Internet access ok          [...]"
else
	echo "[...] Not Connected to Internet   [...]"
	echo "[...] Please check configuration  [...]"
fi
